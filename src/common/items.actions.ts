import {type} from "../common/utils";
import {Action} from "@ngrx/store"; 


export const LOAD = '[Items] load items'
export const LOAD_SUCCESS = '[Items] successfully loaded items'
export const LOAD_FAILURE ='[Items] failed to load items'

export class LoadItemsAction implements Action {
    readonly type = LOAD;
    constructor(public payload:any,public limit:any) {
        console.log(payload)
    }
}

export class LoadItemsFailedAction implements Action { 
    readonly type =  LOAD_FAILURE;
    constructor() {
    }
}

export class LoadItemsSuccessAction implements Action {
    readonly type =  LOAD_SUCCESS;
    constructor(public payload:any) {
    }
}

export type ItemsActions = LoadItemsAction | LoadItemsFailedAction | LoadItemsSuccessAction

















