import { createSelector } from 'reselect';
import {storeLogger} from "ngrx-store-logger";
import * as fromItems from "./items.reducer"
import {compose} from "@ngrx/core";
import {combineReducers, State} from "@ngrx/store";
import {state} from "@angular/core";

export interface AppState {
    items: fromItems.State
}

export const reducers = {
    items: fromItems.reducer
};

const developmentReducer:Function = compose(storeLogger(), combineReducers)(reducers);


export function metaReducer(state: any, action: any) {
  return developmentReducer(state, action);
}


export const getItemsState = (state: AppState) => state.items;
export const getItemsEntities = createSelector(getItemsState, fromItems.getEntities);
export const getItemsCount = createSelector(getItemsState, fromItems.getCount);
export const getItemsPage = createSelector(getItemsState, fromItems.getPage);
export const getItemsLimit= createSelector(getItemsState, fromItems.getLimit);
export const getItemsLoadingState = createSelector(getItemsState, fromItems.getLoadingState);







