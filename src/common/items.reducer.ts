import * as items from './items.actions';

export interface State {
  loaded: boolean;
  loading: boolean;
  entities: Array<any>;
  count: number;
  page: number;
  limit:number
};

const initialState:State = {
  loaded: false,
  loading: false,
  entities: [],
  count: 0,
  page: 1,
  limit:2,
};

export function reducer(state = initialState, action: items.ItemsActions): State {
  switch (action.type) {

    case items.LOAD: {
      const page = action.payload;
      const limit = action.limit;
      return Object.assign({}, state, {
        loading: true,
        page: page == null ? state.page : page,
        limit:limit,
      });
    }

    case items.LOAD_SUCCESS: {
      const items = action.payload['data'];
      const itemsCount = action.payload['_links'].total;
      return Object.assign({}, state ,{
        loaded: true,
        loading: false,
        entities: items,
        count: itemsCount,
      });
    }

    case items.LOAD_FAILURE: {
      return Object.assign({}, state ,{
        loaded: true,
        loading: false,
        entities:[],
        count: 0,
      });
    }
    default:
      return state;
  }

}

export const getEntities = (state:State) =>  state.entities;
export const getPage = (state:State) => state.page;
export const getLimit = (state:State) => state.limit;
export const getCount = (state:State) => state.count;
export const getLoadingState = (state:State) => state.loading;
