import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/switchMap';
import { Observable } from 'rxjs/Observable';
import { Injectable } from "@angular/core";
import * as items from "./items.actions";
import { Actions, Effect } from "@ngrx/effects";
import { AuthProvider } from "../providers/auth/auth";
import { LoadItemsSuccessAction } from "./items.actions";
import { LoadItemsFailedAction } from "./items.actions";

@Injectable()
export class ItemsEffects {
    token1: any;
    constructor(private _actions: Actions, private _service: AuthProvider) {
        this.getListDevices();
    }


    getListDevices() {
        this._service.getToken().subscribe(data => {
            this.token1 = data.access_token;
        }, (err) => {
            console.log(err)
        });
    }


    @Effect() loadItems$ = this._actions.ofType(items.LOAD)
        .switchMap(() => this._service.getListado(this.token1)
            .map((items) => {
                return new LoadItemsSuccessAction(items)
            })).catch(() => Observable.of(new LoadItemsFailedAction())
        );
}
