import {Component, OnInit, Input, EventEmitter, Output} from '@angular/core';
import { NavController } from 'ionic-angular';
import {DetailItemPage} from '../pages/detail-item/detail-item'

@Component({
  selector: 'items-list',
  templateUrl: 'items-list.template.html',

})
export class ItemsListComponent  {
  @Input() items:any;
  @Input() count:number;
  @Input() page:number;
  @Input() limit:number;
  @Input() loading:boolean;
  @Output() onPageChanged = new EventEmitter<number>();
  public state:string;
  constructor( public navCtrl: NavController) {
    this.state = 'list';
  }

  detailsItem(item){
    this.navCtrl.push(DetailItemPage, {
      'item': item,
    });
  }
}