import { Injectable, Inject } from '@angular/core';
import {Http, Headers} from "@angular/http";
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Platform } from 'ionic-angular';
import { Store } from "@ngrx/store";
import * as fromRoot from "../../common/index"

@Injectable()
export class AuthProvider {
  public limit: number;
  public page: number;
  token:any;
  constructor(private store: Store<fromRoot.AppState>, public http: Http, public platform: Platform) {
    store.select(fromRoot.getItemsPage).subscribe((page) => {
      this.page = page;
    });

    store.select(fromRoot.getItemsLimit).subscribe((limit) => {
      this.limit = limit;
    });
  }


  getToken() {
    let url23 = 'https://api.smartenit.io:443/v2/oauth2/token';
    return Observable.create(observer => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Cache-Control', 'no-cache');
      let data_binary = { "grant_type": 'password', "username": 'ionictest@smartenit.com', "password": 'Smartenit@@13', "client_secret": 'afnJPwBNtQIx90u2VyHHYVmSadbMsR4uIdbMXGWtk3mt', "client_id": 'iV1gkyvgQ3MsS9TgHNW6oe' };
      this.http.post(url23 + '?_format=json', data_binary, { headers: headers }).map(res => res.json()).subscribe(data => {
        this.token = data;
        observer.next(this.token);
      }, (err) => {
        observer.error(err);
      });
    });
  }

  getListado(token) {
    let pagination = this.paginate(this.page);
    let url = `https://api.smartenit.io:443/v2/devices/?limit=${pagination.limit}&fields=name,model,state,hwId,media&page=${pagination.offset}`
    return Observable.create(observer => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      if(token==='' || !token){
        headers.append('Authorization', 'Basic ' + this.token.access_token);
      }else{
        headers.append('Authorization', 'Basic ' + token);
      }
      this.http.get(url, { headers: headers }).map(res => res.json()).subscribe(data => {
        observer.next(data);
      }, (err) => {
        observer.error(err);
      });
    });
  }

  paginate(page: number) {
    return {
      offset: page,
      limit: this.limit
    }
  }

}
 
