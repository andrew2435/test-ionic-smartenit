import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Platform, AlertController } from 'ionic-angular';

@Component({
  selector: 'page-detail-item',
  templateUrl: 'detail-item.html',
})
export class DetailItemPage {
  item: any = [];
  constructor(public platform: Platform, private loadingCtrl: LoadingController, private alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams) {
    this.cargarDatos();
  }

  cargarDatos() {
    let loading = this.loadingCtrl.create({
      content: `<div class="custom-spinner-container">
                   <div class="custom-spinner-box"></div>
               </div>`,
    });
    loading.present();
    this.item = this.navParams.get('item');
    console.log(this.item);
    loading.dismiss();
  }

  volver() {
    this.navCtrl.popToRoot();
  }

}
