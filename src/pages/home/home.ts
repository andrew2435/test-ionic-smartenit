import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { NavController,LoadingController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { Http, Headers } from '@angular/http';
import { Store } from "@ngrx/store";
import { Observable } from "rxjs";
import * as fromRoot from '../../common/index';
import * as items from '../../common/items.actions';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  error: string = '';
  token1: any;
  p: number = 1;
  limit:number;

  public items$: Observable<any>;
  public itemsCount$: Observable<number>;
  public itemsPage$: Observable<number>;
  public itemsLimit$: Observable<number>;
  public itemsSize$: Observable<number>;
  public itemsLoading$: Observable<boolean>;

  constructor(private loadingCtrl: LoadingController,public store: Store<fromRoot.AppState>, public http: Http, private empleadoService: AuthProvider, public navCtrl: NavController) {
    this.items$ = this.store.select(fromRoot.getItemsEntities);
    this.itemsCount$ = this.store.select(fromRoot.getItemsCount);
    this.itemsPage$ = this.store.select(fromRoot.getItemsPage);
    this.itemsLoading$ = this.store.select(fromRoot.getItemsLoadingState);
    this.itemsLimit$ = this.store.select(fromRoot.getItemsLimit);
    // this.itemsSize$ = this.store.select(fromRoot.getItemsSize);
    this.limit=4;
  }

  ngOnInit() {
    let loading = this.loadingCtrl.create({
      content: `<div class="custom-spinner-container">
                   <div class="custom-spinner-box"></div>
               </div>`,
    });
    loading.present();
    this.empleadoService.getToken().subscribe(data => {
      this.token1 = data.access_token;
      this.store.dispatch(new items.LoadItemsAction(1,this.limit));
      loading.dismiss();
    }, err1 => {
      loading.dismiss();
      this.error = err1;
      console.log(err1)
    });
  }

  onItemsPageChanged(page: number) {
    let loading = this.loadingCtrl.create({
      content: `<div class="custom-spinner-container">
                   <div class="custom-spinner-box"></div>
               </div>`,
    });
    loading.present();
    this.store.dispatch(new items.LoadItemsAction(page,this.limit))
    loading.dismiss();
  }

  sendLimit(limit){
    let loading = this.loadingCtrl.create({
      content: `<div class="custom-spinner-container">
                   <div class="custom-spinner-box"></div>
               </div>`,
    });
    loading.present();
    this.empleadoService.getToken().subscribe(data => {
      this.token1 = data.access_token;
      console.log("Limit: "+ limit);
      if(limit<=5 && limit>=1){
        this.store.dispatch(new items.LoadItemsAction(1,limit));
      }else{
        alert('Limit between 1 to 5')
        this.store.dispatch(new items.LoadItemsAction(1,this.limit));
      } 
      loading.dismiss();
    }, err1 => {
      loading.dismiss();
      this.error = err1;
      console.log(err1)
    });
    
  }

}